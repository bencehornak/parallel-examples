#define CL_HPP_MINIMUM_OPENCL_VERSION 110
#define CL_HPP_TARGET_OPENCL_VERSION 110
#define CL_HPP_ENABLE_EXCEPTIONS

#include <iostream>
#include <vector>
#include <CL/cl2.hpp>

int main() {
    try {
        std::vector<cl::Platform> platforms;
        cl::Platform::get(&platforms);

        for (auto &platform : platforms) {
            std::cout << platform.getInfo<CL_PLATFORM_NAME>() << " "
                      << platform.getInfo<CL_PLATFORM_VERSION>() << " "
                      << platform.getInfo<CL_PLATFORM_VENDOR>() << std::endl;
        }
    } catch(cl::Error& error) {
        std::cerr << error.what() << " (" << error.err() << ")" << std::endl;
    }
    return 0;
}