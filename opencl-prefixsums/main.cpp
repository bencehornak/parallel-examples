#define CL_HPP_ENABLE_EXCEPTIONS
#define CL_HPP_TARGET_OPENCL_VERSION 110
#define CL_HPP_MINIMUM_OPENCL_VERSION 110

#include <CL/cl2.hpp>
#include <string>
#include <iostream>
#include <chrono>
#include "../benchmark/benchmark.hpp"


const std::string SOURCE = R"(
void swap(local int** a, local int** b) {
    local int* tmp = *a;
    *a = *b;
    *b = tmp;
}

kernel void prefixSums(global const int *array,
                       global int *result,
                       local int *prevRow,
                       local int *currentRow) {
    int gid = get_global_id(0);
    int lid = get_local_id(0);
    int group_size = get_local_size(0);

    prevRow[lid] = array[gid];
    barrier(CLK_GLOBAL_MEM_FENCE);
    // if(lid == 0 && gid > 0) {
    //     prevRow[lid] +=result[gid-1];
    // }

    for(int s = 1; s < group_size; s <<= 1) {
        if(lid - s >= 0)
            currentRow[lid] = prevRow[lid] + prevRow[lid - s];
        else
            currentRow[lid] = prevRow[lid];

        swap(&currentRow, &prevRow);
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    result[gid] = prevRow[lid];
}
)";

const int LENGTH = 1 << 22;
int array[LENGTH];
int result[LENGTH]; // naive
int result2[LENGTH];
const size_t ARRAY_SIZE = sizeof(array);

const size_t LOCAL_WORKERS = 256;
const size_t LOCAL_ARRAY_SIZE = LOCAL_WORKERS * sizeof(int);

cl::Program *program;
cl::KernelFunctor<cl::Buffer, cl::Buffer, cl::Buffer, cl::Buffer> *functor;
cl::Buffer *arrayBuffer;
cl::Buffer *resultBuffer;
cl::Buffer *prevRowBuffer;
cl::Buffer *currentRowBuffer;

void genArray() {
    for (int i = 0; i < LENGTH; ++i) {
        array[i] = i * i;
    }
}


void naive() {
    for (int i = 0; i < LENGTH; ++i) {
        result[i] += array[i];
    }
}

void initOpenCL() {

    program = new cl::Program(SOURCE);
    try {
        program->build();
    } catch (cl::Error &error) {
        std::cerr << program->getBuildInfo<CL_PROGRAM_BUILD_LOG>(cl::Device::getDefault()) << std::endl;
        throw error;
    }
    functor = new cl::KernelFunctor<cl::Buffer, cl::Buffer, cl::Buffer, cl::Buffer>(*program, "prefixSums");

    arrayBuffer = new cl::Buffer(array, array + LENGTH, true, true);
    resultBuffer = new cl::Buffer(0, ARRAY_SIZE);
    prevRowBuffer = new cl::Buffer(0, LOCAL_ARRAY_SIZE);
    currentRowBuffer = new cl::Buffer(0, LOCAL_ARRAY_SIZE);
}

void withOpenCL() {


    (*functor)(
            cl::EnqueueArgs(cl::NDRange(LENGTH), cl::NDRange(LOCAL_WORKERS)),
            *arrayBuffer,
            *resultBuffer,
            *prevRowBuffer,
            *currentRowBuffer
    );

    cl::finish();
    //cl::copy(*resultBuffer, result2, result2 + LENGTH);

}

int main() {

    try {

        genArray();
        initOpenCL();

        benchmark(naive, withOpenCL, 100, 10, 10);
    } catch (cl::Error &err) {
        std::cerr << err.what() << " (" << err.err() << ")" << std::endl;
    }

    /*for (int i : result) {
        std::cout << i << std::endl;
    }*/
}