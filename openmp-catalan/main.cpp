#include <iostream>
#include "../benchmark/benchmark.hpp"

template<class T>
void swap(T &a, T &b) {
    T tmp = a;
    a = b;
    b = tmp;
}


const int n = 10000;
int *prevArray, *curArray;

template<class T>
void test(T functor) {
    stopperStart();
    for (int i = 0; i < 10; ++i)
        functor();
    stopperStop();
}

void parallel();

void serial();

int main() {


    prevArray = new int[n];
    curArray = new int[n];

    benchmark(serial, parallel, 100, 10, 10);


    delete[] prevArray;
    delete[] curArray;


    return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void parallel() {

    prevArray[0] = 1;

    for (int diag = 1; diag < 2 * n + 1; ++diag) {

        int startX = diag < n ? 0 : diag - n + 1;
        int endX = diag / 2;

#pragma omp parallel for schedule(static, 256)
        for (int x = startX; x <= endX; ++x) {

            int y = diag - x;

            int valLeft = x > 0 ? prevArray[x - 1] : 0;
            int valUp = x < y ? prevArray[x] : 0;

            curArray[x] = valLeft + valUp;
        }
        swap(prevArray, curArray);
    }

    // std::cout << prevArray[n - 1] << std::endl;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void serial() {

    prevArray[0] = 1;

    for (int diag = 1; diag < 2 * n + 1; ++diag) {

        int startX = diag < n ? 0 : diag - n + 1;
        int endX = diag / 2;

//#pragma omp parallel for
        for (int x = startX; x <= endX; ++x) {

            int y = diag - x;

            int valLeft = x > 0 ? prevArray[x - 1] : 0;
            int valUp = x < y ? prevArray[x] : 0;

            curArray[x] = valLeft + valUp;
        }
        swap(prevArray, curArray);
    }

    // std::cout << prevArray[n - 1] << std::endl;

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////